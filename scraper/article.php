<?php
require_once (__DIR__ . '/url.php');
//use PHPHtmlParser\Dom;

class article extends url {

     static function download($url) {
       global $client;
       $response =$client->request('GET',$url);
       if($response->getStatusCode() != 200) {

       }
       return  (string)$response->getBody();
    }

     static function parse($html) {

      // $dom = new Dom;
       //$dom->load($html);
       $dom = str_get_html($html);

       $css = ['summary' => 'meta[name="description"], meta[name="twitter:description"], meta[property="og:description"]',
                'title' => 'meta[name="title"], meta[name="twitter:title"], meta[property="og:title"]',
                'published_at' => '.date, .Timestamp',
                'body' => '.canvas-body, .Content-Body'
              ];
        $info = [];
        foreach($css as $key=>$v)
        {
           $d1 = $dom->find($v,0);
           $content = "";
          // $tags = array('</p>','<br />','<br>','<hr />','<hr>','</h1>','</h2>','</h3>','</h4>','</h5>','</h6>');
           if($d1)
           {
            // $str = html_entity_decode(strip_tags(str_replace($tags,"\n",$d1->innerHtml)));
            // $str = preg_replace('/[^\x20-\x7E]/', '', $str);
            //$str = strip_tags($d1->innerHtml);
               $str = $d1->plaintext;
               $content = preg_match('/summary|title/', $key) ? html_entity_decode($d1->content,ENT_QUOTES) : $str;//$d1->getAttribute("content") : $str;
            } else {
            //  echo "count 0" . " " . $key;
            }
            $info[$key] = $content;
        }
        return $info;
    }
}
