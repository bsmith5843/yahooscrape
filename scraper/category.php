<?php
require_once (__DIR__ . '/url.php');

class category extends url {

     static function download($url) {
      $html = phantomselenium($url);
      return $html;
    }

     static function parse($html) {
      $dom = new Dom;
      $dom->load($html);
      $s = ".tile-container.tile > a, .lead-summary > h3 > a, .body-wrap > h3 > a";
      $a = $dom->find($s,0);
      $b = array_map(function($b){return $b->getAttribute("href");}, $a);
      return $b;

    }

}
