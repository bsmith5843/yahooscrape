<?php

require_once (__DIR__ . '/url.php');


class googlesearch extends url {

  static function download($url) {
    global $client;
    $response =$client->request('GET',$url);
    if($response->getStatusCode() != 200) {
       return '';
    }
    sleep(2);
    return  (string)$response->getBody();
 }

  static function parse($html) {
    $links = [];
    $linkObjs = str_get_html($html)->find('h3.r a');
    foreach ($linkObjs as $linkObj) {
      $title = trim($linkObj->plaintext);
      $link = trim($linkObj->href);

      // if it is not a direct link but url reference found inside it, then extract
      if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches) && preg_match('/^https?/', $matches[1])) {
          $link = $matches[1];
      } else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
          continue;
      }
      $links []= $link;
    }
    return $links;
  }

      //    $dom = new Dom;
  //  $dom->load($html);




}
